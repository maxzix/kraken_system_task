Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function removeDuplicates(arr){
    let unique_array = []
    for(let i = 0;i < arr.length; i++){
        if(unique_array.indexOf(arr[i]) == -1){
            unique_array.push(arr[i])
        }
    }
    return unique_array
}

var visited_users = [];

if(getCookie('visited')){
	visited_users = JSON.parse(getCookie('visited'));
}else{
	visited_users = [];
}





function create_li_user(id, name){
	var li = document.createElement('li');
	var id_p = document.createElement('p');
	var name_p = document.createElement('p');

	id_p.innerHTML = id;
	name_p.innerHTML = name;

	li.appendChild(id_p);
	li.appendChild(name_p);
	li.setAttribute('id','git-user-'+id);


	li.addEventListener("click", git_user_details);

	return li;
}

function github_popup_view_changer(view){
	var wrapper = document.getElementById('github-popup');

	var view_1 = wrapper.querySelector('#github-popup-view-1');
	var view_2 = wrapper.querySelector('#github-popup-view-2');

	var spinner = wrapper.querySelector('#github-popup-loader');

	if(view == 1){
		spinner.style.display = 'none';
		view_2.style.display = 'none';
		view_1.style.display = 'block';
	}else if(view == 2){
		spinner.style.display = 'none';
		view_1.style.display = 'none';
		view_2.style.display = 'block';
	}
}
function github_popup_spinner(){
	var wrapper = document.querySelector('github-popup');

	var view_1 = wrapper.querySelector('#github-popup-view-1');
	var view_2 = wrapper.querySelector('#github-popup-view-2');
	var spinner = wrapper.querySelector('#github-popup-loader');

	view_1.style.display = 'none';
	view_2.style.display = 'none';
	spinner.style.display = 'block';
}

function git_user_details(){
	var id = this.id.replace('git-user-','');

	var url = 'https://api.github.com/user/'+id;
	var request = new XMLHttpRequest();
   	request.open('GET', url, true);

   	request.onprogress = function(){
   		github_popup_spinner();
   	};

   	request.onload = function() {
     	if (this.readyState == 4 && this.status == 200) {
     		github_popup_view_changer(2);
       		var user_json = JSON.parse(request.responseText);
       		show_git_user(user_json);
     	} else {
       		
     	}
   	};
   	request.onerror = function() {
     // some error
   	};
   	request.send();
}

function git_popup_back(id){

	var view_1 = document.getElementById("github-popup-view-1");
	var view_2 = document.getElementById("github-popup-view-2");
	view_2.innerHTML = '';
	view_2.style.display = 'none';
	view_1.style.display = 'block';

	visited_users.push(id);
	setCookie('visited',JSON.stringify(visited_users));

	github_popup_view_changer(1);

	document.getElementById('git-user-'+id).remove();
}

function show_git_user(json){
	

	var view_2 = document.getElementById("github-popup-view-2");
	
	var header = document.createElement('h1');
	header.innerHTML = json.login;
	view_2.appendChild(header);

	var image_div = document.createElement('div');
	var image = document.createElement('img');
	image.src = json.avatar_url;
	image_div.appendChild(image);
	view_2.appendChild(image_div);

	var company_div = document.createElement('div');
	var company = document.createElement('p');
	company.innerHTML = 'Company:';
	var company_name = document.createElement('p');
	if(json.company == null){
		company_name.innerHTML = 'No company';
	}else{
		company_name.innerHTML = json.company;
	}
	company_div.appendChild(company);
	company_div.appendChild(company_name);
	company_div.className += 'github-company';
	view_2.appendChild(company_div);

	var stats_div = document.createElement('div');
	stats_div.className += 'github-stats';
	var stats = document.createElement('h2');
	stats.innerHTML = 'Stats';
	stats_div.appendChild(stats);

	var repos = document.createElement('p');
	repos.innerHTML = 'Repos: '+json.public_repos;
	var gists = document.createElement('p');
	gists.innerHTML = 'Gists: '+json.public_gists;
	
	stats_div.appendChild(repos);
	stats_div.appendChild(gists);

	view_2.appendChild(stats_div);

	var back_btn = document.createElement('button');
	back_btn.className += 'github-popup-back';
	back_btn.innerHTML = 'Go back';
	back_btn.onclick = function() { git_popup_back(json.id); }
	view_2.appendChild(back_btn);

}

var load_users_from = 0;

function github_load_users(from){

	var view_1 = document.getElementById('github-popup-view-1');
	var list = view_1.getElementsByTagName("ul")[0];

	var url = 'https://api.github.com/users?since='+from;
	var request = new XMLHttpRequest();
   	request.open('GET', url, true);

   	request.onload = function() {
     	if (request.status >= 200 && request.status < 400) {
       		var users_json = JSON.parse(request.responseText);
       		for(var i = 0;i<users_json.length;i++){
       			var current = users_json[i];
       			if(visited_users.length == 0){
       				var user_li = create_li_user(current.id, current.login);
       				list.appendChild(user_li);
       			}else{
       				for(var y = 0; y<visited_users.length; y++){
       					if(visited_users.includes(current.id)){
       						break;
       					}else{
       						var user_li = create_li_user(current.id, current.login);
       						list.appendChild(user_li);
       						break;
       					}
       				}
       			}
       			if(i == users_json.length-1){
       				load_users_from = String(users_json[i].id);
       			}
       		}
     	} else {
       // some error
     	}
   	};
   	request.onerror = function() {
     // some error
   	};
   	request.send();
}
function close_github_popup(){
	var div = document.getElementById('github-popup');
	div.style.display = 'none';
}
function initialize() {

	var main = document.createElement("div");
	main.setAttribute('id','github-popup');
	main.className += 'github-popup';
	document.body.appendChild(main);

	var wrapper = document.createElement("div");
	wrapper.className += 'wrapper' ;
	main.appendChild(wrapper);

	var view_1 = document.createElement("div");
	view_1.setAttribute('id','github-popup-view-1');
	wrapper.appendChild(view_1);

	var header = document.createElement('h1');
	header.innerHTML = 'Github Users';
	view_1.appendChild(header);

	var view_2 = document.createElement("div");
	view_2.setAttribute('id','github-popup-view-2');
	wrapper.appendChild(view_2);

	var list = document.createElement('ul');
	view_1.appendChild(list);
	var loader = document.createElement('div');
	loader.className += 'spinner';
	loader.setAttribute('id','github-popup-loader');

	for (var i = 1; i < 6; i++) {
		var block = document.createElement('div');
		block.className += 'rect'+i;
		loader.appendChild(block);
	}

	wrapper.appendChild(loader);

	github_load_users(load_users_from);

	var load_more = document.createElement('button');
	load_more.innerHTML = 'Load more';
	load_more.style.width = '100%';
	load_more.onclick = function() { github_load_users(load_users_from); }
	view_1.appendChild(load_more);

	var dismiss_popup_div = document.createElement('div');
	dismiss_popup_div.className += 'exit_popup';

	var dismiss_popup = document.createElement('button');
	dismiss_popup.onclick = close_github_popup;
	dismiss_popup.innerHTML = 'X';

	dismiss_popup_div.appendChild(dismiss_popup);
	wrapper.appendChild(dismiss_popup_div);

}

$(document).ready(initialize);
